const express = require('express'),
      http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const config = require('./app/config');

const trackRouter = require('./routes/trackRouter');
const countRouter = require('./routes/countRouter');
const rootRouter = require('./routes/rootRouter');

const hostname = config.hostname;
const port = config.port;

const app = express();

//don't show the log when it is test
if(process.env.NODE_ENV !== 'test') {
    //use morgan to log at command line
    app.use(morgan('combined')); //'combined' outputs the Apache style LOGs
}

app.use(bodyParser.json());                                     

app.use('/track', trackRouter);
app.use('/count', countRouter);
app.use('/', rootRouter);

const server = http.createServer(app);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

module.exports = app; // for testing