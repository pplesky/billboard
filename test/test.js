//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

var assert = require('assert');


//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
chai.use(chaiHttp);

describe('GET /count', () => {
    it('it should GET the count as object', (done) => {
      chai.request(server)
          .get('/count')
          .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('count');
            done();
          });
    });
});

describe('GET /', () => {
  it('it should return error 403', (done) => {
    chai.request(server)
        .get('/')
        .end((err, res) => {
            res.should.have.status(403);
        done();
        });
  });
});

describe('GET /track/xxxxxxxxx', () => {
  it('it should return error 403', (done) => {
    chai.request(server)
        .get('/')
        .end((err, res) => {
            res.should.have.status(403);
        done();
        });
  });
});

describe('POST /track', () => {
  it('it should return status 201 Created', (done) => {
    chai.request(server)
    .post('/track')
    .send({'name': 'Test', 'count': '10'})
    .end(function(err, res){
      res.should.have.status(201);
    done();
    });
  });
});