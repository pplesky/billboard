module.exports = function(requestId) {
    // this object will be serialized to JSON and sent in the body of the request
    // loadtest http://localhost:3000/track -c 100 -m POST -T application/json -n 10000 -p test\testdata.js
    return {
      count: 1,
      time: new Date(),
      requestId: requestId
    };
  };
