const config = {
    // file for json output
    filename: 'test.txt',

    // redis counter key
    key: "counter",

    // express configuration
    hostname: 'localhost',
    port: 3000
};

module.exports = config;