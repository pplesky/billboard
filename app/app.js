const bluebird = require('bluebird');
const redis = require("redis");
bluebird.promisifyAll(redis);
const fs = bluebird.promisifyAll(require('fs'));
const config = require('../app/config');

class MyApp {

    constructor() {
        this.client = redis.createClient();
        this.fs = fs;
    }

    appendToFile(text) {
        return this.fs.appendFileAsync(config.filename, text + '\n');
    }

    getCount() {
        return this.client.getAsync(config.key);
    }

    incrementCount(count) {
        return this.client.incrbyAsync(config.key, count);
    }

    setCount(count) {
        return this.client.setAsync(config.key, count);
    }

}

const myApp = new MyApp();

module.exports = myApp;