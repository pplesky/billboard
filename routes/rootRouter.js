const express = require('express');
const rootRouter = express.Router();

rootRouter.route("/**")

.all((req, res, next) => {
    res.statusCode = 403;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Unsupported method');
});

module.exports = rootRouter;
