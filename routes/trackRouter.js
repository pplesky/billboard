const express = require('express');
const bodyParser = require('body-parser');
const myApp = require('../app/app');
const config = require('../app/config');

const trackRouter = express.Router();
trackRouter.use(bodyParser.json());

trackRouter.route('/')

.post((req, res, next) => {
    let json = JSON.stringify(req.body);
    
    let p1 = myApp.appendToFile(json);
    let p2 = ("count" in req.body) ? myApp.incrementCount(req.body.count) : Promise.resolve();

    Promise.all([p1, p2])
    
    .then(() => {
        res.statusCode = 201;
        res.setHeader('Content-Type', 'text/plain');
        res.end("Accepted");
    })

    .catch((err) => {
        console.log(err);
        next(err);
    });
});


module.exports = trackRouter;
