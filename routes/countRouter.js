const express = require('express');
const bodyParser = require('body-parser');
const myApp = require('../app/app');

const countRouter = express.Router();
countRouter.use(bodyParser.json());

countRouter.route('/')

.get((req, res, next) => {
    myApp.getCount()
    .then((count) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({count: count});
    }, (err) => {
        next(err);
    });
});

module.exports = countRouter;